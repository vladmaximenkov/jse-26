package ru.vmaksimenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.repository.ICommandRepository;
import ru.vmaksimenkov.tm.command.AbstractCommand;

import java.util.*;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> arguments = new TreeMap<>();

    @NotNull
    private final Map<String, AbstractCommand> commands = new TreeMap<>((o1, o2) -> {
        if (o1.contains("-") && !o2.contains("-")) return -1;
        if (o2.contains("-") && !o1.contains("-")) return 1;
        return o1.compareTo(o2);
    });

    @Override
    public void add(@NotNull final AbstractCommand command) {
        @Nullable final String arg = command.arg();
        @NotNull final String name = command.name();
        if (arg != null) arguments.put(arg, command);
        commands.put(name, command);
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @NotNull
    @Override
    public Collection<String> getCommandArgs() {
        @NotNull final List<String> result = new ArrayList<>();
        commands.values().stream()
                .filter(e -> !isEmpty(e.arg()))
                .forEach(e -> result.add(e.arg()));
        return result;
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArg(@NotNull final String name) {
        return arguments.get(name);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return commands.get(name);
    }

    @NotNull
    @Override
    public Collection<String> getCommandNames() {
        @NotNull final List<String> result = new ArrayList<>();
        commands.values().stream()
                .filter(e -> !isEmpty(e.name()))
                .forEach(e -> result.add(e.name()));
        return result;
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

}
