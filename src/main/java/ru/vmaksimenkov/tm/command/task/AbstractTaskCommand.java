package ru.vmaksimenkov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.command.AbstractCommand;
import ru.vmaksimenkov.tm.enumerated.Role;
import ru.vmaksimenkov.tm.exception.entity.TaskNotFoundException;
import ru.vmaksimenkov.tm.model.Task;

import static ru.vmaksimenkov.tm.util.TerminalUtil.dashedLine;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

    protected void showTask(@Nullable final Task task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        if (task.getStatus() != null)
            System.out.println("STATUS: " + task.getStatus().getDisplayName());
        System.out.println("PROJECT ID: " + task.getProjectId());
        System.out.println("CREATED: " + task.getCreated());
        System.out.println("STARTED: " + task.getDateStart());
        System.out.println("FINISHED: " + task.getDateFinish());
        System.out.print(dashedLine());
    }

}
