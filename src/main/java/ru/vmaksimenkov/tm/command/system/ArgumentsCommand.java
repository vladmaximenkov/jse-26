package ru.vmaksimenkov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ArgumentsCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all arguments";
    }

    @Override
    public void execute() {
        System.out.println("[ARGUMENTS]");
        @Nullable final Collection<AbstractCommand> arguments = serviceLocator.getCommandService().getArguments();
        assert arguments != null;
        arguments.forEach(e -> System.out.println(e.arg()));
    }

    @NotNull
    @Override
    public String name() {
        return "arguments";
    }

}
